package film

import (
	"context"
	"go-bioskop-api/repository/dto"
)

func (f *film) Create(ctx context.Context, film dto.FilmRequest) (dto.FilmRequest, error) {
	var filmId int
	query := "INSERT INTO films (theater_id, name, genre, schedule, duration, price, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id"
	err := f.Db.QueryRow(query, film.TheaterId, film.Name, film.Genre, film.Schedule, film.Duration, film.Price, film.CreatedAt, film.UpdatedAt).Scan(&filmId)
	if err != nil {
		return film, err
	}

	film.Id = filmId

	return film, nil
}

func (f *film) GetAll(ctx context.Context) ([]dto.FilmResponseList, error) {
	var filmListData []dto.FilmResponseList
	query := "SELECT films.id, films.name, theaters.code, films.genre, films.schedule, films.duration, films.price, theaters.capacity FROM films INNER JOIN theaters ON films.theater_id = theaters.id ORDER BY films.schedule DESC"
	rows, err := f.Db.QueryContext(ctx, query)
	if err != nil {
		return filmListData, err
	}

	for rows.Next() {
		filmData := dto.FilmResponseList{}
		rows.Scan(&filmData.Id, &filmData.Name, &filmData.Theater, &filmData.Genre, &filmData.Schedule, &filmData.Duration, &filmData.Price, &filmData.Capacity)
		filmListData = append(filmListData, filmData)
	}

	return filmListData, nil
}

func (f *film) GetById(ctx context.Context, filmId int) (dto.FilmResponseList, error) {
	var filmData dto.FilmResponseList
	query := "SELECT films.id, films.name, theaters.code, films.genre, films.schedule, films.duration, films.price, theaters.capacity FROM films INNER JOIN theaters ON films.theater_id = theaters.id WHERE films.id = $1 ORDER BY films.schedule DESC"
	rows, err := f.Db.QueryContext(ctx, query, filmId)
	if err != nil {
		return filmData, err
	}

	if rows.Next() {
		err = rows.Scan(&filmData.Id, &filmData.Name, &filmData.Theater, &filmData.Genre, &filmData.Schedule, &filmData.Duration, &filmData.Price, &filmData.Capacity)
		if err != nil {
			return filmData, err
		}
	}

	return filmData, nil
}
