package middleware

import (
	"go-bioskop-api/repository/user"
	"go-bioskop-api/services/auth"
	"net/http"
)

type Middleware interface {
	AuthMiddleware(next http.Handler) http.Handler
	AuthMiddlewareAdmin(next http.Handler) http.Handler
}

type authMiddleware struct {
	user user.Repository
	auth auth.Service
}

func NewAuthMiddleware(user user.Repository, auth auth.Service) Middleware {
	return &authMiddleware{user: user, auth: auth}
}
