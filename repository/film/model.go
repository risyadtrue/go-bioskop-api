package film

import (
	"context"
	"database/sql"
	"go-bioskop-api/repository/dto"
)

type Repository interface {
	Create(ctx context.Context, film dto.FilmRequest) (dto.FilmRequest, error)
	GetAll(ctx context.Context) ([]dto.FilmResponseList, error)
	GetById(ctx context.Context, filmId int) (dto.FilmResponseList, error)
}

type film struct {
	Db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return &film{Db: db}
}
