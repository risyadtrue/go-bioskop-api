package controller

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi"
	"go-bioskop-api/helper"
	"go-bioskop-api/repository"
	"go-bioskop-api/repository/dto"
	"go-bioskop-api/services"
	"net/http"
	"strconv"
)

func OrderTicket(w http.ResponseWriter, r *http.Request) {
	var orderTicketRequest dto.OrderTicketRequest
	err := json.NewDecoder(r.Body).Decode(&orderTicketRequest)
	ctx := context.Background()

	filmIdParams := chi.URLParam(r, "filmId")
	filmId, err := strconv.Atoi(filmIdParams)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	userData := r.Context().Value("user").(dto.UserRequest)

	orderTicketRequest.FilmId = filmId
	orderTicketRequest.UserId = userData.Id

	service := services.ServiceApplication.Order
	orderTicketResponse, err := service.Order(ctx, orderTicketRequest)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(w, r, orderTicketResponse)
}

func ListOrder(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	repo := repository.ApplicationData.Order
	listOrder, err := repo.GetAll(ctx)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(w, r, listOrder)
}
