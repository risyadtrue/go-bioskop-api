package order

import (
	"context"
	"database/sql"
	"go-bioskop-api/repository/dto"
)

type Repository interface {
	Create(ctx context.Context, order dto.OrderTicketRequest) (dto.OrderTicketResponse, error)
	GetAll(ctx context.Context) ([]dto.OrderAllTicketResponse, error)
}

type order struct {
	Db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return &order{Db: db}
}
