package theater

import (
	"context"
	"database/sql"
	"go-bioskop-api/repository/dto"
)

type Repository interface {
	GetAll(ctx context.Context) ([]dto.TheaterResponse, error)
}

type theater struct {
	Db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return &theater{Db: db}
}
