package auth

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
)

var SECRET_KEY = []byte("LogkarManiaMantap")

func (s *auth) GenerateToken(userId int, role string) (string, error) {
	claim := jwt.MapClaims{}
	// add to token payload
	claim["userId"] = userId
	claim["role"] = role

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	signedToken, err := token.SignedString(SECRET_KEY)
	if err != nil {
		return signedToken, nil
	}

	return signedToken, nil
}

func (s *auth) ValidateToken(token string) (*jwt.Token, error) {
	tkn, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)

		if !ok {
			return nil, errors.New("Invalid Token")
		}

		return SECRET_KEY, nil
	})

	if err != nil {
		return tkn, nil
	}

	return tkn, nil
}
