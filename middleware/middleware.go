package middleware

import (
	"go-bioskop-api/repository"
	"go-bioskop-api/services"
)

type MiddlewareStruct struct {
	Auth Middleware
}

var userRepository = repository.ApplicationData.User
var authService = services.ServiceApplication.Auth

var ApplicationDataMiddleware = MiddlewareStruct{
	Auth: NewAuthMiddleware(userRepository, authService),
}
