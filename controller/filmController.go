package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"go-bioskop-api/helper"
	"go-bioskop-api/repository"
	"go-bioskop-api/repository/dto"
	"net/http"
	"time"
)

func CreateFilm(w http.ResponseWriter, r *http.Request) {
	fmt.Print("create film nih boss")
	ctx := context.Background()
	var filmRequest dto.FilmRequest
	err := json.NewDecoder(r.Body).Decode(&filmRequest)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	filmRequest.CreatedAt = time.Now()
	filmRequest.UpdatedAt = time.Now()

	repo := repository.ApplicationData.Film
	newFilm, err := repo.Create(ctx, filmRequest)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(w, r, newFilm)
}

func ListFilm(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	repo := repository.ApplicationData.Film
	filmList, err := repo.GetAll(ctx)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	helper.HttpResponseSuccess(w, r, filmList)
}
