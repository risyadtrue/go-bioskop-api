package theater

import (
	"context"
	"go-bioskop-api/repository/dto"
)

func (t *theater) GetAll(ctx context.Context) ([]dto.TheaterResponse, error) {
	var theaters []dto.TheaterResponse
	query := "SELECT id, code, available, created_at, updated_at FROM theaters"
	rows, err := t.Db.QueryContext(ctx, query)
	if err != nil {
		return theaters, nil
	}

	for rows.Next() {
		var theaterData dto.TheaterResponse
		err = rows.Scan(&theaterData.Id, &theaterData.Code, &theaterData.Available, &theaterData.CreatedAt, &theaterData.UpdatedAt)
		if err != nil {
			return theaters, err
		}

		theaters = append(theaters, theaterData)
	}

	return theaters, nil

}
