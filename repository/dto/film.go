package dto

import "time"

type FilmRequest struct {
	Id        int       `json:"id"`
	TheaterId int       `json:"theater_id"`
	Name      string    `json:"name"`
	Genre     string    `json:"genre"`
	Schedule  time.Time `json:"schedule"`
	Duration  int       `json:"duration"`
	Price     int       `json:"price"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type FilmResponseList struct {
	Id       int       `json:"id"`
	Name     string    `json:"name"`
	Theater  string    `json:"theater"`
	Genre    string    `json:"genre"`
	Schedule time.Time `json:"schedule"`
	Duration int       `json:"duration"`
	Price    int       `json:"price"`
	Capacity int       `json:"capacity"`
}
