package controller

import (
	"context"
	"encoding/json"
	"errors"
	"go-bioskop-api/helper"
	"go-bioskop-api/repository"
	"go-bioskop-api/repository/dto"
	"go-bioskop-api/services"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"time"
)

func RegisterUser(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	var userRegisterRequest dto.UserRequest
	err := json.NewDecoder(r.Body).Decode(&userRegisterRequest)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(userRegisterRequest.Password), bcrypt.MinCost)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	userRegisterRequest.Password = string(passwordHash)
	userRegisterRequest.CreatedAt = time.Now()
	userRegisterRequest.UpdatedAt = time.Now()
	userRegisterRequest.Role = "user"

	repo := repository.ApplicationData.User
	user, err := repo.Create(ctx, userRegisterRequest)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	service := services.ServiceApplication.Auth
	token, err := service.GenerateToken(user.Id, user.Role)
	user.Token = token

	helper.HttpResponseSuccess(w, r, user)
}

func LoginUser(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	var userLogin dto.UserLoginRequest
	err := json.NewDecoder(r.Body).Decode(&userLogin)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	repo := repository.ApplicationData.User
	userLoggedIn, err := repo.FindByEmail(ctx, userLogin.Email)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusInternalServerError)
		return
	}

	if userLoggedIn.Id == 0 {
		helper.HttpResponseError(w, r, errors.New("User not found for this email"), http.StatusNotFound)
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(userLoggedIn.Password), []byte(userLogin.Password))
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	service := services.ServiceApplication.Auth
	token, err := service.GenerateToken(userLoggedIn.Id, userLoggedIn.Role)

	userLoggedInData := dto.UserLoggedInResponse{
		Id:        userLoggedIn.Id,
		Name:      userLoggedIn.Name,
		Email:     userLoggedIn.Email,
		Role:      userLoggedIn.Role,
		Token:     token,
		CreatedAt: userLoggedIn.CreatedAt,
	}
	helper.HttpResponseSuccess(w, r, userLoggedInData)
}
