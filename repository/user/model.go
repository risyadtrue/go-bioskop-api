package user

import (
	"context"
	"database/sql"
	"go-bioskop-api/repository/dto"
)

type Repository interface {
	Create(ctx context.Context, user dto.UserRequest) (dto.UserLoggedInResponse, error)
	FindByEmail(ctx context.Context, email string) (dto.UserRequest, error)
	FindById(ctx context.Context, userId int) (dto.UserRequest, error)
}

type user struct {
	Db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return &user{Db: db}
}
