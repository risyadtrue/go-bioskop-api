package services

import (
	"go-bioskop-api/services/auth"
	"go-bioskop-api/services/order"
)

type Services struct {
	Auth  auth.Service
	Order order.Services
}

var ServiceApplication = Services{
	Auth:  auth.NewService(),
	Order: order.NewService(),
}
