package order

import (
	"context"
	"go-bioskop-api/repository/dto"
)

type Services interface {
	Order(ctx context.Context, request dto.OrderTicketRequest) ([]dto.OrderTicketResponse, error)
}

type orderService struct {
}

func NewService() Services {
	return &orderService{}
}
