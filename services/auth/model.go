package auth

import "github.com/dgrijalva/jwt-go"

type Service interface {
	GenerateToken(userId int, role string) (string, error)
	ValidateToken(token string) (*jwt.Token, error)
}

type auth struct {
}

func NewService() Service {
	return &auth{}
}
