package order

import (
	"context"
	"go-bioskop-api/repository"
	"go-bioskop-api/repository/dto"
)

func (s *orderService) Order(ctx context.Context, request dto.OrderTicketRequest) ([]dto.OrderTicketResponse, error) {
	var orderDataResponses []dto.OrderTicketResponse

	filmRepo := repository.ApplicationData.Film
	filmData, err := filmRepo.GetById(ctx, request.FilmId)
	if err != nil {
		return orderDataResponses, err
	}

	request.Total = request.Quantity * filmData.Price
	orderRepo := repository.ApplicationData.Order
	_, err = orderRepo.Create(ctx, request)
	if err != nil {
		return orderDataResponses, err
	}

	for i := 0; i < request.Quantity; i++ {
		orderDataResponse := dto.OrderTicketResponse{
			Film:    filmData.Name,
			Theater: filmData.Theater,
			Price:   filmData.Price,
		}

		orderDataResponses = append(orderDataResponses, orderDataResponse)
	}

	return orderDataResponses, nil
}
