package order

import (
	"context"
	"go-bioskop-api/repository/dto"
)

func (o *order) Create(ctx context.Context, order dto.OrderTicketRequest) (dto.OrderTicketResponse, error) {
	var orderId int
	var orderTickerResponse dto.OrderTicketResponse
	query := "INSERT INTO orders (user_id, film_id, quantity, total, created_at, updated_at) VALUES ($1, $2, $3, $4, NOW(), NOW()) RETURNING id"
	err := o.Db.QueryRow(query, order.UserId, order.FilmId, order.Quantity, order.Total).Scan(&orderId)
	if err != nil {
		return orderTickerResponse, err
	}

	return orderTickerResponse, nil
}

func (o *order) GetAll(ctx context.Context) ([]dto.OrderAllTicketResponse, error) {
	var orderListData []dto.OrderAllTicketResponse
	query := "SELECT users.name, films.name, orders.quantity, orders.total, orders.created_at FROM orders INNER JOIN users ON users.id = orders.user_id INNER JOIN films ON films.id = orders.film_id"
	rows, err := o.Db.QueryContext(ctx, query)
	if err != nil {
		return orderListData, err
	}

	for rows.Next() {
		orderData := dto.OrderAllTicketResponse{}
		rows.Scan(&orderData.Name, &orderData.Film, &orderData.Quantity, &orderData.Total, &orderData.CreatedAt)
		orderListData = append(orderListData, orderData)
	}

	return orderListData, nil
}
