module go-bioskop-api

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	github.com/joho/godotenv v1.4.0
)

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/lib/pq v1.10.4
	golang.org/x/crypto v0.0.0-20220307211146-efcb8507fb70
)
