package user

import (
	"context"
	"errors"
	"go-bioskop-api/repository/dto"
)

func (u *user) Create(ctx context.Context, user dto.UserRequest) (dto.UserLoggedInResponse, error) {
	var registeredUser dto.UserLoggedInResponse
	var userId int
	query := "INSERT INTO users (name, email, password, role, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id"
	err := u.Db.QueryRow(query, user.Name, user.Email, user.Password, user.Role, user.CreatedAt, user.UpdatedAt).Scan(&userId)
	if err != nil {
		return registeredUser, err
	}

	registeredUser.Id = userId
	registeredUser.Name = user.Name
	registeredUser.Email = user.Email
	registeredUser.CreatedAt = user.CreatedAt
	registeredUser.Role = user.Role

	return registeredUser, nil
}

func (u *user) FindByEmail(ctx context.Context, email string) (dto.UserRequest, error) {
	var loggedInUser dto.UserRequest
	query := "SELECT id, name, email, password, role, created_at FROM users WHERE email = $1"
	row, err := u.Db.QueryContext(ctx, query, email)
	if err != nil {
		return loggedInUser, err
	}

	if row.Next() {
		err = row.Scan(&loggedInUser.Id, &loggedInUser.Name, &loggedInUser.Email, &loggedInUser.Password, &loggedInUser.Role, &loggedInUser.CreatedAt)
		if err != nil {
			return loggedInUser, err
		}
	} else {
		return loggedInUser, errors.New("user not found")
	}

	return loggedInUser, nil
}

func (u *user) FindById(ctx context.Context, userId int) (dto.UserRequest, error) {
	var loggedInUser dto.UserRequest
	query := "SELECT id, name, email, password, role, created_at FROM users WHERE id = $1"
	row, err := u.Db.QueryContext(ctx, query, userId)
	if err != nil {
		return loggedInUser, err
	}

	if row.Next() {
		err = row.Scan(&loggedInUser.Id, &loggedInUser.Name, &loggedInUser.Email, &loggedInUser.Password, &loggedInUser.Role, &loggedInUser.CreatedAt)
		if err != nil {
			return loggedInUser, err
		}
	} else {
		return loggedInUser, errors.New("user not found")
	}

	return loggedInUser, nil
}
