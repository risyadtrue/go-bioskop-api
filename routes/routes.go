package routes

import (
	"fmt"
	"github.com/go-chi/chi"
	"go-bioskop-api/controller"
	"go-bioskop-api/middleware"
	"log"
	"net/http"
)

func RoutesCollection() {
	mid := middleware.ApplicationDataMiddleware.Auth
	r := chi.NewRouter()
	r.Route("/api/v1", func(r chi.Router) {
		r.Post("/user/register", controller.RegisterUser)
		r.Post("/user/login", controller.LoginUser)
		r.With(mid.AuthMiddlewareAdmin).Get("/theater", controller.ListTheater)
		r.With(mid.AuthMiddlewareAdmin).Post("/film", controller.CreateFilm)
		r.Get("/film", controller.ListFilm)
		r.With(mid.AuthMiddleware).Post("/order/ticket/{filmId}", controller.OrderTicket)
		r.With(mid.AuthMiddlewareAdmin).Get("/order", controller.ListOrder)
	})

	fmt.Println("Running in port localhost:3000")
	log.Fatal(http.ListenAndServe(":3000", r))
}
