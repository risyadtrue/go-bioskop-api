package dto

import "time"

type OrderTicketRequest struct {
	UserId    int `json:"user_id"`
	FilmId    int `json:"film_id"`
	Quantity  int `json:"quantity"`
	Total     int `json:"total"`
	CreatedAt int `json:"created_at"`
	UpdatedAt int `json:"updated_at"`
}

type OrderTicketResponse struct {
	Film      string `json:"film_id"`
	Theater   string `json:"theater"`
	Price     int    `json:"quantity"`
	CreatedAt int    `json:"created_at"`
}

type OrderAllTicketResponse struct {
	Name      string    `json:"name"`
	Film      string    `json:"film"`
	Quantity  int       `json:"quantity"`
	Total     int       `json:"total"`
	CreatedAt time.Time `json:"created_at"`
}
