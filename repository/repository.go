package repository

import (
	_ "github.com/lib/pq"
	"go-bioskop-api/connection"
	"go-bioskop-api/repository/film"
	"go-bioskop-api/repository/order"
	"go-bioskop-api/repository/theater"
	"go-bioskop-api/repository/user"
)

type Repository struct {
	User    user.Repository
	Theater theater.Repository
	Film    film.Repository
	Order   order.Repository
}

var db = connection.PostgresSQLConnection()

var ApplicationData = Repository{
	User:    user.NewRepository(db),
	Theater: theater.NewRepository(db),
	Film:    film.NewRepository(db),
	Order:   order.NewRepository(db),
}
