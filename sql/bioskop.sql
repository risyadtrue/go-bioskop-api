CREATE TABLE users (
   id SERIAL PRIMARY KEY,
   email varchar(100),
   name varchar(100),
   password varchar(100),
   role varchar,
   created_at timestamp,
   updated_at timestamp
)

CREATE TABLE theaters (
  id SERIAL PRIMARY KEY,
  code varchar(100),
  available bool,
  created_at timestamp,
  updated_at timestamp
)

CREATE TABLE films (
   id SERIAL PRIMARY KEY,
   theater_id int,
   name varchar(100),
   genre varchar(100),
   schedule timestamp,
   duration int,
   price int,
   created_at timestamp,
   updated_at timestamp,
   CONSTRAINT fk_theater_id FOREIGN KEY (theater_id) REFERENCES users(id)
)


CREATE TABLE orders (
    id SERIAL PRIMARY KEY,
    user_id int,
    film_id int,
    quantity int,
    total int,
    created_at timestamp,
    updated_at timestamp,
    CONSTRAINT fk_user_order_id FOREIGN KEY (user_id) REFERENCES users(id),
    CONSTRAINT fk_film_order_id FOREIGN KEY (film_id) REFERENCES films(id)
)