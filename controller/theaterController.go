package controller

import (
	"context"
	"go-bioskop-api/helper"
	"go-bioskop-api/repository"
	"net/http"
)

func ListTheater(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	repo := repository.ApplicationData.Theater
	theaterList, err := repo.GetAll(ctx)
	if err != nil {
		helper.HttpResponseError(w, r, err.Error(), http.StatusBadRequest)
		return
	}

	helper.HttpResponseSuccess(w, r, theaterList)
}
