package middleware

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"go-bioskop-api/helper"
	"net/http"
	"strings"
)

func (middleware *authMiddleware) AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")
		ctx := context.Background()

		if !strings.Contains(authHeader, "Bearer") {
			helper.HttpResponseError(writer, request, errors.New("token invalid"), http.StatusUnauthorized)
			return
		}

		tokenSplit := strings.Split(authHeader, " ")
		tokenString := tokenSplit[1]

		token, err := middleware.auth.ValidateToken(tokenString)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusUnauthorized)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusUnauthorized)
			return
		}

		userId := int(claim["userId"].(float64))

		user, err := middleware.user.FindById(ctx, userId)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusInternalServerError)
			return
		}

		ctxValue := context.WithValue(request.Context(), "user", user)
		next.ServeHTTP(writer, request.WithContext(ctxValue))
	})
}

func (middleware *authMiddleware) AuthMiddlewareAdmin(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		authHeader := request.Header.Get("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			helper.HttpResponseError(writer, request, errors.New("invalid token"), http.StatusUnauthorized)
			return
		}

		tokenSplit := strings.Split(authHeader, " ")
		tokenString := tokenSplit[1]

		token, err := middleware.auth.ValidateToken(tokenString)
		if err != nil {
			helper.HttpResponseError(writer, request, err.Error(), http.StatusUnauthorized)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {
			helper.HttpResponseError(writer, request, errors.New("invalid token"), http.StatusUnauthorized)
			return
		}

		role := claim["role"].(string)

		if role != "admin" {
			helper.HttpResponseError(writer, request, errors.New("only admin can access this route"), http.StatusUnauthorized)
			return
		}

		ctxValue := context.WithValue(request.Context(), "role", role)
		next.ServeHTTP(writer, request.WithContext(ctxValue))
	})
}
