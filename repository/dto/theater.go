package dto

import "time"

type TheaterResponse struct {
	Id        int       `json:"id"`
	Code      string    `json:"code"`
	Available bool      `json:"available"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
